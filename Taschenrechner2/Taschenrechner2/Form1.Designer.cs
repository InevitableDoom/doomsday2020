﻿namespace Taschenrechner2
{
    partial class frmtaschenrechner
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblOperator = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblErgebnis = new System.Windows.Forms.Label();
            this.txtOperand1 = new System.Windows.Forms.TextBox();
            this.txtOperand2 = new System.Windows.Forms.TextBox();
            this.btnAddition = new System.Windows.Forms.Button();
            this.btnsubtraktion = new System.Windows.Forms.Button();
            this.btnmittelwert = new System.Windows.Forms.Button();
            this.btnmaximum = new System.Windows.Forms.Button();
            this.btnpotenz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblOperator
            // 
            this.lblOperator.AutoSize = true;
            this.lblOperator.BackColor = System.Drawing.SystemColors.Control;
            this.lblOperator.Location = new System.Drawing.Point(220, 37);
            this.lblOperator.Name = "lblOperator";
            this.lblOperator.Size = new System.Drawing.Size(0, 13);
            this.lblOperator.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(74, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ergebnis:";
            // 
            // lblErgebnis
            // 
            this.lblErgebnis.BackColor = System.Drawing.Color.White;
            this.lblErgebnis.Location = new System.Drawing.Point(339, 98);
            this.lblErgebnis.Name = "lblErgebnis";
            this.lblErgebnis.Size = new System.Drawing.Size(100, 23);
            this.lblErgebnis.TabIndex = 2;
            // 
            // txtOperand1
            // 
            this.txtOperand1.Location = new System.Drawing.Point(77, 34);
            this.txtOperand1.Name = "txtOperand1";
            this.txtOperand1.Size = new System.Drawing.Size(85, 20);
            this.txtOperand1.TabIndex = 3;
            // 
            // txtOperand2
            // 
            this.txtOperand2.Location = new System.Drawing.Point(339, 37);
            this.txtOperand2.Name = "txtOperand2";
            this.txtOperand2.Size = new System.Drawing.Size(100, 20);
            this.txtOperand2.TabIndex = 4;
            // 
            // btnAddition
            // 
            this.btnAddition.Location = new System.Drawing.Point(77, 139);
            this.btnAddition.Name = "btnAddition";
            this.btnAddition.Size = new System.Drawing.Size(85, 23);
            this.btnAddition.TabIndex = 5;
            this.btnAddition.Text = "Addition";
            this.btnAddition.UseVisualStyleBackColor = true;
            this.btnAddition.Click += new System.EventHandler(this.btnAddition_Click);
            // 
            // btnsubtraktion
            // 
            this.btnsubtraktion.Location = new System.Drawing.Point(168, 139);
            this.btnsubtraktion.Name = "btnsubtraktion";
            this.btnsubtraktion.Size = new System.Drawing.Size(85, 23);
            this.btnsubtraktion.TabIndex = 6;
            this.btnsubtraktion.Text = "Subtraktion";
            this.btnsubtraktion.UseVisualStyleBackColor = true;
            this.btnsubtraktion.Click += new System.EventHandler(this.btnsubtraktion_Click);
            // 
            // btnmittelwert
            // 
            this.btnmittelwert.Location = new System.Drawing.Point(77, 168);
            this.btnmittelwert.Name = "btnmittelwert";
            this.btnmittelwert.Size = new System.Drawing.Size(85, 23);
            this.btnmittelwert.TabIndex = 7;
            this.btnmittelwert.Text = "Mittelwert";
            this.btnmittelwert.UseVisualStyleBackColor = true;
            this.btnmittelwert.Click += new System.EventHandler(this.btnmittelwert_Click);
            // 
            // btnmaximum
            // 
            this.btnmaximum.Location = new System.Drawing.Point(77, 197);
            this.btnmaximum.Name = "btnmaximum";
            this.btnmaximum.Size = new System.Drawing.Size(84, 23);
            this.btnmaximum.TabIndex = 8;
            this.btnmaximum.Text = "Maximum";
            this.btnmaximum.UseVisualStyleBackColor = true;
            this.btnmaximum.Click += new System.EventHandler(this.btnmaximum_Click);
            // 
            // btnpotenz
            // 
            this.btnpotenz.Location = new System.Drawing.Point(169, 169);
            this.btnpotenz.Name = "btnpotenz";
            this.btnpotenz.Size = new System.Drawing.Size(84, 23);
            this.btnpotenz.TabIndex = 9;
            this.btnpotenz.Text = "Potenz";
            this.btnpotenz.UseVisualStyleBackColor = true;
            this.btnpotenz.Click += new System.EventHandler(this.btnpotenz_Click);
            // 
            // frmtaschenrechner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnpotenz);
            this.Controls.Add(this.btnmaximum);
            this.Controls.Add(this.btnmittelwert);
            this.Controls.Add(this.btnsubtraktion);
            this.Controls.Add(this.btnAddition);
            this.Controls.Add(this.txtOperand2);
            this.Controls.Add(this.txtOperand1);
            this.Controls.Add(this.lblErgebnis);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblOperator);
            this.Name = "frmtaschenrechner";
            this.Text = "Taschenrechner";
            this.Load += new System.EventHandler(this.frmtaschenrechner_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblOperator;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblErgebnis;
        private System.Windows.Forms.TextBox txtOperand1;
        private System.Windows.Forms.TextBox txtOperand2;
        private System.Windows.Forms.Button btnAddition;
        private System.Windows.Forms.Button btnsubtraktion;
        private System.Windows.Forms.Button btnmittelwert;
        private System.Windows.Forms.Button btnmaximum;
        private System.Windows.Forms.Button btnpotenz;
    }
}

