﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace PingPong
{
    public partial class frmGameOver : Form
    {
        public int score = Frmpingpong.score;
        public string PlayerName;
        public static string resultFile = @"C:\Daten\M404\PingPong\PingPong\bin\Debug\ResultFile.txt";
        

        public frmGameOver()
        {
            InitializeComponent();
        }

        private void BtnSchliessen_Click(object sender, EventArgs e)
        {
            this.Close();
            Frmpingpong.score = 0;
        }

        public void FrmGameOver_Load(object sender, EventArgs e)
        {
            ReadFile(resultFile);
            lblPunkte.Text = Convert.ToString(score);
        }

        public void BtnEintragen_Click(object sender, EventArgs e)
        {
            PlayerName = Convert.ToString(txtName.Text);
            lblErgebnisse.Text = score + " " + PlayerName + " " + DateTime.Now.ToString("dd/MM/yyyy" + Environment.NewLine);
            WriteFile(resultFile);
            ReadFile(resultFile);
        }

        public void WriteFile(string ResultFile)
        {
            
            FileStream fs = new FileStream(ResultFile, FileMode.Append, FileAccess.Write);

            if (fs.CanWrite)
            {
                byte[] buffer = Encoding.ASCII.GetBytes(lblErgebnisse.Text);
                fs.Write(buffer, 0, buffer.Length);
            }

            fs.Flush();
            fs.Close();
        }

        public void ReadFile(string ResultFile)
        {
            FileStream fs = new FileStream(ResultFile, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[1024];
            int bytesread = fs.Read(buffer, 0, buffer.Length);

            lblErgebnisse.Text = Encoding.ASCII.GetString(buffer, 0, bytesread);

            fs.Close();
        }
    }
}
