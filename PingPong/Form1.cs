﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PingPong
{
    public partial class Frmpingpong : Form
    {
        // Eigenschaft der Klasse
        private int ballspeedX = 5;
        private int ballspeedY = 2;
        public static int score = 0;
        readonly Random rndNumber = new Random();
        public static bool hasrun;
        private bool hasRun = false;

        // Ende

        public Frmpingpong()
        {
            InitializeComponent();
        }
        #region Ballsteuerung 
        public void TmrSpiel_Tick(object sender, EventArgs e)
        {
             // Bewegung des Balles
             picBall.Location = new Point(picBall.Location.X + ballspeedX, picBall.Location.Y + ballspeedY);

            // ball berührt den Schläger
            if (picBall.Location.X >= pnlSpiel.Width - picBall.Width - picSchlägerRechts.Width
                && picBall.Location.Y + picBall.Height >= picSchlägerRechts.Location.Y
                && picBall.Location.Y <= picSchlägerRechts.Location.Y + picSchlägerRechts.Height)
            {
                ballspeedX = -ballspeedX;
                score += 10; // Punkte werden addiert
            }

            // Game Over fenster öffnen falls der ball den schläger nicht berührt
            if (picBall.Location.X > pnlSpiel.Width - picBall.Width)
            {
                tmrSpiel.Stop();
                picBall.Location = new Point(pnlSpiel.Width / 2 - picBall.Width / 2, pnlSpiel.Height / 2 - picBall.Height / 2);
                frmGameOver n = new frmGameOver();
                n.Show();

                picBall.BackColor = Color.DeepSkyBlue;
                tmrSpiel.Interval = 110;
                picBall.Location = new Point(picBall.Location.X + ballspeedX, picBall.Location.Y + ballspeedY);
            }

            // bounce des balles
            if (picBall.Location.X > 475)
            {
                ballspeedX = -ballspeedX;
            }

            if (picBall.Location.Y > 275)
            {
                ballspeedY = -ballspeedY;
            }

            if (picBall.Location.X < 0)
            {
                ballspeedX = -ballspeedX;
            }

            if (picBall.Location.Y < 0)
            {
                ballspeedY = -ballspeedY;
            }

            if (rdbBall.Checked == true)
            {
                vsbSchlägerRechts.Enabled = false;
                btnHoch.Enabled = true;
                btnLinks.Enabled = true;
                btnRechts.Enabled = true;
                btnUnten.Enabled = true;
            }
            else
            {
                btnHoch.Enabled = false;
                btnLinks.Enabled = false;
                btnRechts.Enabled = false;
                btnUnten.Enabled = false;
                vsbSchlägerRechts.Enabled = true;
            }
            // Punkte des Labels werden zu String konvertiert
            txtPunkte.Text = Convert.ToString(score);


            if (!hasRun && score > 0 && score % 50 == 0)
            {
                RandomColor();
                tmrSpiel.Interval -= 5;
                hasRun = true;
            }
            else if (score > 0 && score % 50 != 0)
            {
                hasRun = false;
            }
            

        }
        #endregion

        private void BtnStart_Click(object sender, EventArgs e)
        {
            // Timer startet
            tmrSpiel.Start();
        }

        public void Frmpingpong_Load_1(object sender, EventArgs e)
        {
            // Schläger wird an den rechten Rand des Panels plaziert
            picSchlägerRechts.Location = new Point(pnlSpiel.Width - picSchlägerRechts.Width, pnlSpiel.Height / 2);

            vsbSchlägerRechts.Height = pnlSpiel.Height; // Höhe der Scrollbar wird an der Höhe des Panels angepasst
            vsbSchlägerRechts.Location = new Point(pnlSpiel.Location.X + pnlSpiel.Width, pnlSpiel.Location.Y);
            // Scrollbar wird an den rechten Rand des Panels plaziert

            vsbSchlägerRechts.Maximum = pnlSpiel.Height - picSchlägerRechts.Height + vsbSchlägerRechts.LargeChange;
            vsbSchlägerRechts.Value = picSchlägerRechts.Location.Y;

            // Damit die Tastatursteuerung funktioniert muss die KeyPreview auf true gestellt sein
            KeyPreview = true;

        }

        private void VsbSchlägerRechts_Scroll(object sender, ScrollEventArgs e)
        {
            // Die Position des Schlägers wird mit der Value der Scrollbar abgeglichen
            picSchlägerRechts.Location = new Point(picSchlägerRechts.Location.X, vsbSchlägerRechts.Value);
            vsbSchlägerRechts.Value = picSchlägerRechts.Location.Y;
        }
        #region Tastensteuerung
        public void Frmpingpong_Keydown(object sender, KeyEventArgs e)
        {
            // das - vor den variablen bewirkt, dass der wert umgekehrt wird. d.H 5 wird zu -5 und umgekehrt
            if (e.KeyCode == Keys.H)
            {
                ballspeedX = -ballspeedX;
            }

            if (e.KeyCode == Keys.V)
            {
                ballspeedY = -ballspeedY;
            }

            if (e.KeyCode == Keys.P)
            {
                // Timer wird gestoppt
                tmrSpiel.Stop();
            }

            if (e.KeyCode == Keys.S)
            {
                // Timer wird gestartet
                tmrSpiel.Start();
            }
        }
        #endregion
        #region ButtonSteuerung
        private void BtnHoch_Click(object sender, EventArgs e)
        {
            picBall.Location = new Point(picBall.Location.X, picBall.Location.Y - 25);
        }

        private void BtnLinks_Click(object sender, EventArgs e)
        {
            picBall.Location = new Point(picBall.Location.X - 25, picBall.Location.Y);
        }

        private void BtnUnten_Click(object sender, EventArgs e)
        {
            picBall.Location = new Point(picBall.Location.X, picBall.Location.Y + 25);
        }

        private void BtnRechts_Click(object sender, EventArgs e)
        {
            picBall.Location = new Point(picBall.Location.X + 25, picBall.Location.Y);
        }
        #endregion
        #region Zufallsfarbe
        public void RandomColor()
        {
            int Farbe = Convert.ToInt32(rndNumber.Next(1, 11));
            
            if (Farbe == 1)
            {
                picBall.BackColor = Color.Red;
            }
            else if (Farbe == 2)
            {
                picBall.BackColor = Color.Crimson;
            }
            else if (Farbe == 3)
            {
                picBall.BackColor = Color.PaleVioletRed;
            }
            else if (Farbe == 4)
            {
                picBall.BackColor = Color.DeepPink;
            }
            else if (Farbe == 5)
            {
                picBall.BackColor = Color.Azure;
            }
            else if (Farbe == 6)
            {
                picBall.BackColor = Color.Black;
            }
            else if (Farbe == 7)
            {
                picBall.BackColor = Color.DodgerBlue;
            }
            else if (Farbe == 8)
            {
                picBall.BackColor = Color.DarkGoldenrod;
            }
            else if (Farbe == 9)
            {
                picBall.BackColor = Color.AntiqueWhite;
            }
            else if (Farbe == 10)
            {
                picBall.BackColor = Color.SandyBrown;
            }
        }
        #endregion
    }
}
